//'use strict';

$(function() {


    /*
    |--------------------------------------------------------------------------
    | Select2
    |--------------------------------------------------------------------------
    */

    $(".select2").each(function() {
        $(this).select2({
            placeholder: $(this).attr('placeholder'),
            width: '100%',
            minimumResultsForSearch: Infinity,
        });
    });

    $(".select2-search").each(function() {
        $(this).select2({
            placeholder: $(this).attr('placeholder'),
            width: '100%',
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Smooth Scroll
    |--------------------------------------------------------------------------
    */

    $('.page-scroll').on('click', function(event) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            let target = $(this.hash),
                speed = $(this).data("speed") || 800;
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 180
                }, speed);
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Sticky Kit
    |--------------------------------------------------------------------------
    */

    $(".js-sticky").stick_in_parent({
        offset_top: 65,
    });

    /*
    |--------------------------------------------------------------------------
    |  Disable body scrolling when modal is open IOS only
    |--------------------------------------------------------------------------
    */

    $(".modal").on("shown.bs.modal", function () {
        $("body").addClass("modal-open");
    }).on("hidden.bs.modal", function () {
        $("body").removeClass("modal-open")
    });

    /*
    |--------------------------------------------------------------------------
    | // https://www.coralnodes.com/hide-header-on-scroll-down/
    |--------------------------------------------------------------------------
    */

    if ($('.float-button').length > 0) {

        var doc = document.documentElement;
        var w = window;

        var prevScroll = w.scrollY || doc.scrollTop;
        var curScroll;
        var direction = 0;
        var prevDirection = 0;
        var headerHeight = $(".float-button").height();

        var header = document.getElementById('float-button');

        var checkScroll = function() {

            /*
            ** Find the direction of scroll
            ** 0 - initial, 1 - up, 2 - down
            */

            curScroll = w.scrollY || doc.scrollTop;
            if (curScroll > prevScroll) {
                //scrolled up
                direction = 2;
            }
            else if (curScroll < prevScroll) {
                //scrolled down
                direction = 1;
            }

            if (direction !== prevDirection) {
                toggleHeader(direction, curScroll);
            }

            prevScroll = curScroll;
        };

        var toggleHeader = function(direction, curScroll) {
            if (direction === 2 && curScroll > headerHeight) {
                header.classList.add('is-hidden');
                prevDirection = direction;
            }
            else if (direction === 1) {
                header.classList.remove('is-hidden');
                prevDirection = direction;
            }
        };

        window.addEventListener('scroll', checkScroll);

    }

    /*
    |--------------------------------------------------------------------------
    | Mobile Menu
    |--------------------------------------------------------------------------
    */

    var menuClose = $('.js-menu-close');

    menuClose.on('click', function(e) {

        $('body').removeClass('mmenu-open');
    });


    var burger = $('.js-menu-trigger');

    // Sidebar toggle to sidebar-folded
    burger.on('click', function(e) {

        e.preventDefault();

        $('body').toggleClass('mmenu-open');

    });

    // close sidebar when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of sidebar menu when clicking outside of it
        if (!$(e.target).closest(burger).length) {
            var sidebar = $(e.target).closest('.m-menu').length;
            var sidebarBody = $(e.target).closest('.m-menu__body').length;
            if (!sidebar && !sidebarBody) {
                if ($('body').hasClass('mmenu-open')) {
                    $('body').removeClass('mmenu-open');
                }
            }
        }
    });


   /*
   |--------------------------------------------------------------------------
   | Showing modal with effect
   |--------------------------------------------------------------------------
   */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

    /*
    |--------------------------------------------------------------------------
    | Entry Slider
    |--------------------------------------------------------------------------
    */

	let entrySlider = new Swiper('.js-entry-slider', {
		speed: 800,
		mousewheel: false,
		loop: true,
		autoplay: {
			delay: 2500,
			disableOnInteraction: false,
		},
		spaceBetween: 2,
        pagination: {
            el: '.js-entry-slider-pagination',
            clickable: true,
        },
        touchRatio: 1,
		slidesPerView: 1,
	});

    /*
    |--------------------------------------------------------------------------
    | Polyfill object-fit/object-position on <img>: IE9, IE10, IE11, Edge, Safari, ...
    | https://github.com/bfred-it/object-fit-images
    |--------------------------------------------------------------------------
    */

    objectFitImages();
    // if you use jQuery, the code is: $(function () { objectFitImages() });

});
